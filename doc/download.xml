<?xml version="1.0"?>
<document url="./download.xml">

<properties>
    <title>Downloading - The Apache Struts Web Application Framework</title>
    <author>Martin Cooper</author>
</properties>

<body>

    <section name="Downloading Struts" href="downloading">

    <p>
    Use the links below to download Apache Struts from one of our mirrors. You
    <strong>must</strong> <a href="#verify">verify the integrity</a> of the
    downloaded files using signatures downloaded from our main distribution
    directory.
    </p>

        <section name="Mirror" href="mirror">

        <p>
        [if-any logo]
            <a href="[link]"><img align="right" src="[logo]" border="0" alt="logo" /></a>
        [end]
        The currently selected mirror is <b>[preferred]</b>. If you encounter
        a problem with this mirror, please select another mirror. If all
        mirrors are failing, there are <i>backup</i> mirrors (at the end of
        the mirrors list) that should be available.
        </p>

        <form action="[location]" method="get" id="SelectMirror">
        Other mirrors:
        <select name="Preferred">
        [if-any http]
            [for http]<option value="[http]">[http]</option>[end]
        [end]  
        [if-any ftp]
            [for ftp]<option value="[ftp]">[ftp]</option>[end]
        [end]
        [if-any backup]
            [for backup]<option value="[backup]">[backup] (backup)</option>[end]
        [end]
        </select>
        <input type="submit" value="Change" />     
        </form>

        <p>
        You may also consult the
        <a href="http://www.apache.org/mirrors/">complete list of mirrors</a>.
        </p>

        </section>

        <section name="Struts 1.2.2" href="struts122">

        <p>
        Struts 1.2.2 is the latest production release of Struts. It is available
        in a binary distribution, a source distribution, and a minimal library
        distribution.
        </p>

        <ul>

        <li>Binaries:
            <ul>
                <li>
                <a href="[preferred]/struts/binaries/jakarta-struts-1.2.2.zip">jakarta-struts-1.2.2.zip</a> 
                [<a href="http://www.apache.org/dist/struts/binaries/jakarta-struts-1.2.2.zip.asc">PGP</a>]
                [<a href="http://www.apache.org/dist/struts/binaries/jakarta-struts-1.2.2.zip.md5">MD5</a>]
                </li>
                <li>
                <a href="[preferred]/struts/binaries/jakarta-struts-1.2.2.tar.gz">jakarta-struts-1.2.2.tar.gz</a> 
                [<a href="http://www.apache.org/dist/struts/binaries/jakarta-struts-1.2.2.tar.gz.asc">PGP</a>]
                [<a href="http://www.apache.org/dist/struts/binaries/jakarta-struts-1.2.2.tar.gz.md5">MD5</a>]
                </li>
            </ul>
        </li>

        <li>Source:
            <ul>
                <li>
                <a href="[preferred]/struts/source/jakarta-struts-1.2.2-src.zip">jakarta-struts-1.2.2-src.zip</a> 
                [<a href="http://www.apache.org/dist/struts/source/jakarta-struts-1.2.2-src.zip.asc">PGP</a>]
                [<a href="http://www.apache.org/dist/struts/source/jakarta-struts-1.2.2-src.zip.md5">MD5</a>]
                </li>
                <li>
                <a href="[preferred]/struts/source/jakarta-struts-1.2.2-src.tar.gz">jakarta-struts-1.2.2-src.tar.gz</a> 
                [<a href="http://www.apache.org/dist/struts/source/jakarta-struts-1.2.2-src.tar.gz.asc">PGP</a>]
                [<a href="http://www.apache.org/dist/struts/source/jakarta-struts-1.2.2-src.tar.gz.md5">MD5</a>]
                </li>
            </ul>
        </li>

        <li>Library:
            <ul>
                <li>
                <a href="[preferred]/struts/library/jakarta-struts-1.2.2-lib.zip">jakarta-struts-1.2.2-lib.zip</a> 
                [<a href="http://www.apache.org/dist/struts/library/jakarta-struts-1.2.2-lib.zip.asc">PGP</a>]
                [<a href="http://www.apache.org/dist/struts/library/jakarta-struts-1.2.2-lib.zip.md5">MD5</a>]
                </li>
                <li>
                <a href="[preferred]/struts/library/jakarta-struts-1.2.2-lib.tar.gz">jakarta-struts-1.2.2-lib.tar.gz</a> 
                [<a href="http://www.apache.org/dist/struts/library/jakarta-struts-1.2.2-lib.tar.gz.asc">PGP</a>]
                [<a href="http://www.apache.org/dist/struts/library/jakarta-struts-1.2.2-lib.tar.gz.md5">MD5</a>]
                </li>
            </ul>
        </li>

        </ul>

        </section>

        <section name="Verify the integrity of the files" href="verify">

        <p>
        It is essential that you verify the integrity of the downloaded files
        using the PGP or MD5 signatures.
        </p>

        <p>
        The PGP signatures can be verified using PGP or GPG.  First download
        the <a href="http://www.apache.org/dist/struts/KEYS">KEYS</a> as well
        as the <code>asc</code> signature file for the particular distribution.
        Make sure you get these files from the
        <a href="http://www.apache.org/dist/struts/">main distribution directory</a>,
        rather than from a mirror. Then verify the signatures using
        </p>

        <p>
        <code>
        % pgpk -a KEYS<br />
        % pgpv ${filename}.tar.gz.asc<br />
        </code>
        <em>or</em><br />
        <code>
        % pgp -ka KEYS<br />
        % pgp ${filename}.tar.gz.asc<br />
        </code>
        <em>or</em><br />
        <code>
        % gpg --import KEYS<br />
        % gpg --verify ${filename}.tar.gz.asc
        </code>
        </p>

        <p>
        Alternatively, you can verify the MD5 signature on the files. A Unix
        program called <code>md5</code> or <code>md5sum</code> is included in
        many Unix distributions. It is also available as part of
        <a href="http://www.gnu.org/software/textutils/textutils.html">GNU
        Textutils</a>. Windows users can get binary md5 programs from
        <a href="http://www.fourmilab.ch/md5/">here</a>,
        <a href="http://www.pc-tools.net/win32/freeware/console/">here</a>, or
        <a href="http://www.slavasoft.com/fsum/">here</a>.
        </p>

        </section>

    </section>

</body>
</document>
