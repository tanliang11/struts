<%
/**
 * Redirect default requests to Welcome action.
 * @version $Revision: 1.2 $ $Date: 2004/01/18 13:43:07 $
*/
%>
<%@ page language="java" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<logic:forward name="welcome"/>
