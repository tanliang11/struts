-[BOARD REPORT] 21-Jul-2004 Apache Struts-

We have started a reorganization of our repository. The goals of the refactoring are to better support subprojects  with their own release cycles and building Struts with Apache Maven.

An initial draft of the reorganization is being done under Subversion on a private server, with all discussions taking place on the public DEV list. We will be ready to move the work to an Apache server soon, now that we have a consensus in favor of Subversion and Maven.

We completed a draft of Apache Struts bylaws and developer guidelines, which is available at <http://struts.apache.org/bylaws.html>.

There was a discussion on the DEV list regarding the "bar" for Committership. The consensus is to keep the bar set fairly high and wait until a contributor has submitted a good number of useful patches directly to Struts.

Our latest stable release is still 1.1 (29 June 2003). We issued a 1.2.1 release on 11 July 2004, which is currently catagorized as a beta. We anticipate 1.2.1 (or a 1.2.2) being promoted to GA over the next 30 days.

----

-[STATUS LOG] 20-Jul-2004 Apache Struts-

* PMC Members
* Other Committers
* PMC Actions
* Significant Threads
* Releases
* Roadmap
* Mailing List Subscriptions
* Wiki Posts
* CVS Activity
* Issue Reports
* Showstoppers
* Recent Changes

----

-PMC Actions-

* Started reorganization of repository. The goals of the refactoring are to better support

  * Subprojects   
  * Maven builds
  
  The reorganization is being done under Subversion on an offsite server. We will be moving this to an Apache server soon, now that we have a consensus in favor of Subversion and Maven. 

* Completed draft of Apache Struts bylaws: http://struts.apache.org/bylaws.html


-Significant threads-

* We discussed the "bar" for Committership. The consensus is to keep the bar fairly high and wait until a Committer has submitted a number of useful patches to Struts directly, rather than to related projects elsewhere. 


-Releases-

* Stable release: 1.1 (29 June 2003).

* Beta release: 1.2.1 (11 July 2004)

* We anticipate 1.2.1 or 1.2.2 being promoted to GA in the next 30 days. 


-Roadmap-

* We are discussing renumbering coding initatives, so that the current reorganization under SVN becomes 2.0, but that is undecided. Heretofore, the plans has been:

  * Struts 1.x will remain based on Servlet 1.2/JSP 1.1 (evolution).

  * Struts 1.3.x will introduce the "Struts Chain" request processor. Some packages, like the taglibs, will be released as separate subprojects.

  * Struts 2.x will be based on Servlet 2.4/JSP 2.0 (revolution).

* For more see <http://jakarta.apache.org/struts/roadmap.html>.


-Showstoppers-

* A stable 1.1.3 release of the Commons Validator (anticipated).


-Recent Changes-

* Many changes to Struts Faces in anticipate of a 1.0 release.

* Added a simplified build.properties file for use by developers who are not actively involved in Jakata Commons development.

* Extended "EmptyTag" to support arrays. 

* See also: http://jakarta.apache.org/struts/userGuide/release-notes.html


-Mailing list Subscriptions-

* User 1838
* Dev: 719
* PMC: 14


-Wiki Posts-

* 148 new posts; 323 total (since Apr 8)


-CVS Activity-

* Timeframe: 56 days, Total Commits: 84 Total Number of Files Changed: 196


-Issue Reports-

* Open "problem" reports:      10 [#27332 .. #30204]
* Open "enhancement" reports: 261 [#  866 .. #30210] (28 with patch)
* Open "LATER" reports:        19 [#17977 .. #26403]


-PMC Members-

    * Craig R. McClanahan (craigmcc at apache.org)
    * Ted Husted (husted at apache.org)
    * Rob Leland (rleland at apache.org)
    * Cedric Dumoulin (cedric.dumoulin at lifl.fr)
    * Martin Cooper (martinc at apache.org)
    * Arron Bates (arron at apache.org)
    * James Holmes (jholmes at apache.org)
    * David M. Karr (dmkarr at apache.org)
    * David Graham (dgraham at apache.org)
    * James Mitchell (jmitchell at apache.org)
    * Steve Raeburn (sraeburn at apache.org)
    * Don Brown (mrdon at apache.org)
    * Joe Germuska (germuska at apache.org)


-Other Committers-

    * Niall Pemberton (niallp at apache.org)


$Id: STATUS.txt,v 1.14 2004/07/22 13:31:53 husted Exp $
###
